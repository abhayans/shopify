package com.abhayan.app.shopifyexample1;

/**
 * Created by abhayan on 11/23/2017.
 */

public final class Config {

        public static final boolean DEBUG = Boolean.parseBoolean("true");
        public static final String APPLICATION_ID = "com.abhayan.app.shopifyexample1";
        public static final String BUILD_TYPE = "debug";
        public static final String FLAVOR = "";
        public static final int VERSION_CODE = 1;
        public static final String VERSION_NAME = "1.0";


}
