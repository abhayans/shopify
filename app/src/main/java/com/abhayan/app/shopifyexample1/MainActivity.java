package com.abhayan.app.shopifyexample1;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import com.shopify.buy3.GraphCall;
import com.shopify.buy3.GraphClient;
import com.shopify.buy3.GraphError;
import com.shopify.buy3.GraphResponse;
import com.shopify.buy3.HttpCachePolicy;
import com.shopify.buy3.QueryGraphCall;
import com.shopify.buy3.Storefront;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        GraphClient graphClient = GraphClient.builder(this)
                .shopDomain("feel-destination-gift-shop.myshopify.com")
                .accessToken("550af97bb3c2727255b28180b279ae23")
                .httpCache(new File(getApplicationContext().getCacheDir(), "/http"), 10 * 1024 * 1024)
                .defaultHttpCachePolicy(HttpCachePolicy.CACHE_FIRST.expireAfter(5, TimeUnit.MINUTES))
                .build();

        Storefront.QueryRootQuery query = Storefront.query(rootQuery -> rootQuery
                .shop(shopQuery -> shopQuery
                        .collections(arg -> arg.first(10), collectionConnectionQuery -> collectionConnectionQuery
                                .edges(collectionEdgeQuery -> collectionEdgeQuery
                                        .node(collectionQuery -> collectionQuery
                                                .title()
                                                .products(arg -> arg.first(10), productConnectionQuery -> productConnectionQuery
                                                        .edges(productEdgeQuery -> productEdgeQuery
                                                                .node(productQuery -> productQuery
                                                                        .title()
                                                                        .productType()
                                                                        .description()
                                                                )
                                                        )
                                                )
                                        )
                                )
                        )
                )
        );

        QueryGraphCall call = graphClient.queryGraph(query);

        call.enqueue(new GraphCall.Callback<Storefront.QueryRoot>() {


            @Override
            public void onResponse(@NonNull GraphResponse<Storefront.QueryRoot> response) {
                List<Storefront.Collection> collections = new ArrayList<>();
                for (Storefront.CollectionEdge collectionEdge : response.data().getShop().getCollections().getEdges()) {
                    collections.add(collectionEdge.getNode());
                    System.out.println("main_collections: " + collectionEdge.getNode().getTitle());

                    List<Storefront.Product> products = new ArrayList<>();
                    for (Storefront.ProductEdge productEdge : collectionEdge.getNode().getProducts().getEdges()) {
                        products.add(productEdge.getNode());
                        System.out.println("main_products: " + productEdge.getNode().getTitle());
                    }

                }
            }

            @Override
            public void onFailure(GraphError error) {

            }
        });


    }
}
